package utils

import (
	"testing"
)

func TestGetPreviousSequence(t *testing.T) {
	//t.Parallel()
	t.Run("Normal Case", func(t *testing.T) {
		inputPreviousSequence := []int{5, 10, 20000, 64000, 64999, 65000}
		outputPreviousSequence := []int{4, 9, 19999, 63999, 64998, 64999}
		for i := 0; i < len(inputPreviousSequence); i++ {
			if GetPreviousSequence(inputPreviousSequence[i]) != outputPreviousSequence[i] {
				t.Errorf("\n previous ( %v ) != %v", inputPreviousSequence[i], outputPreviousSequence[i])
			}
		}
	})

	t.Run("Corner Case", func(t *testing.T) {
		inputPreviousSequence := []int{1}
		outputPreviousSequence := []int{65000}
		for i := 0; i < len(inputPreviousSequence); i++ {
			if GetPreviousSequence(inputPreviousSequence[i]) != outputPreviousSequence[i] {
				t.Errorf("\n previous ( %v ) != %v", inputPreviousSequence[i], outputPreviousSequence[i])
			}
		}
	})
}

func TestGetNextSequence(t *testing.T) {
	//t.Parallel()
	t.Run("Normal Case", func(t *testing.T) {
		inputNextSequence := []int{1, 4, 10, 20000, 64000, 64999}
		outputNextSequence := []int{2, 5, 11, 20001, 64001, 65000}
		for i := 0; i < len(inputNextSequence); i++ {
			if GetNextSequence(inputNextSequence[i]) != outputNextSequence[i] {
				t.Errorf("\n next (%v) != %v \n", inputNextSequence[i], outputNextSequence[i])
			}
		}
	})

	t.Run("Corner Case", func(t *testing.T) {
		inputNextSequence := []int{65000}
		outputNextSequence := []int{1}
		for i := 0; i < len(inputNextSequence); i++ {
			if GetNextSequence(inputNextSequence[i]) != outputNextSequence[i] {
				t.Errorf("\n next (%v) != %v \n", inputNextSequence[i], outputNextSequence[i])
			}
		}
	})
}

func TestDistanceBetweenSequenceNumber(t *testing.T) {
	//t.Parallel()
	t.Run("Normal Case", func(t *testing.T) {
		inputDistanceSequenceSource := []int{4, 10, 64000, 65000, 10, 30000, 40000}
		inputDistanceSequenceDestination := []int{10, 100, 64000, 63000, 64990, 40000, 30000}
		outputDistanceSequence := []int{6, 90, 0, -2000, -20, 10000, -10000}
		for i := 0; i < len(inputDistanceSequenceSource); i++ {
			if DistanceBetweenSequenceNumber(inputDistanceSequenceSource[i], inputDistanceSequenceDestination[i]) != outputDistanceSequence[i] {
				t.Errorf("\n Distance ( %v,%v) != %v \n", inputDistanceSequenceSource[i], inputDistanceSequenceDestination[i], outputDistanceSequence[i])
			}
		}
	})

	t.Run("Corner Case", func(t *testing.T) {
		inputDistanceSequenceSource := []int{20000, 64990, 64990, 10}
		inputDistanceSequenceDestination := []int{64999, 100, 10, 64990}
		outputDistanceSequence := []int{-20001, 110, 20, -20}
		for i := 0; i < len(inputDistanceSequenceSource); i++ {
			if DistanceBetweenSequenceNumber(inputDistanceSequenceSource[i], inputDistanceSequenceDestination[i]) != outputDistanceSequence[i] {
				t.Errorf("\n Distance ( %v,%v) != %v \n", inputDistanceSequenceSource[i], inputDistanceSequenceDestination[i], outputDistanceSequence[i])
			}
		}
	})
}

func TestSortedSequenceInsert(t *testing.T) {
	//t.Parallel()
	arrIn := []int{10, 5, 64990, 65000, 64999, 3, 1, 20, 2000, 63000, 50000, 40000}
	sortedOut := []int{40000, 50000, 63000, 64990, 64999, 65000, 1, 3, 5, 10, 20, 2000}
	var arrOut []int
	for i := 0; i < len(arrIn); i++ {
		SortedSequenceInsert(&arrOut, arrIn[i])
	}
	for i := 0; i < len(arrOut); i++ {
		if arrOut[i] != sortedOut[i] {
			t.Errorf("\n arrOut[%v] != sortedOut[%v] %v != %v \n", i, i, arrOut[i], sortedOut[i])
		}
	}
}
