package utils

import (
	"errors"
	"fmt"
	"log"
)

// Let's say we have sequence starting from 1 and highest valid sequence is 10, then the sequence will be
// 1, 2, ... 7, 8, 9, 10, 1 ... and so on. We have to implement simple functions so that we can easily get distance,
// latestSequence and others from simple input of sequence numbers.

const MaxValidSequence int = 65000
const MinValidSequence int = 1

// input shouldn't be out of range. valid sequence is from  to maxValidSequence
func checkValidInputSequence(args ...int) (bool, string) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Got Panic in checkValidInputSequence:", r)
			log.Println("Got Panic in checkValidInputSequence:", r)
		}
	}()

	invalidFinal := false
	msgFinal := ""
	for _, val := range args {
		invalid, msg := isSequenceInvalid(val)
		if invalid {
			//fmt.Printf("invalid sequence number: %v, expected [ %v - %v ]\n", val, MinValidSequence, MaxValidSequence)
			//warn := fmt.Sprintf("invalid sequence number: %v, expected [ %v - %v ]", val, MIN_VALID_SEQUENCE, MAX_VALID_SEQUENCE)
			//panic(warn)
			invalidFinal = invalidFinal || invalid
			msgFinal = msgFinal + msg.Error()
		}
	}
	if invalidFinal {
		return false, msgFinal
	}
	return true, ""
}

// Check if the given sequence number is invalid.
// @param x - sequence number to be checked for validation.
// @return - true, if sequence number is invalid, otherwise, return - false.
func isSequenceInvalid(x int) (bool, error) {
	if x < MinValidSequence || x > MaxValidSequence {
		return true, errors.New(fmt.Sprintf("invalid sequence number: %v, expected [ %v - %v ]\n", x, MinValidSequence, MaxValidSequence))
	} else {
		return false, nil
	}
}

// GetPreviousSequence
// Calculates the possible previous sequence of given current sequence
// @param - currentSequence The value of current sequence, for which previous sequence need to be calculated.
// @return - possible previous sequence of given #currentSequence
func GetPreviousSequence(seq int) int {
	valid, msg := checkValidInputSequence(seq)
	if !valid {
		fmt.Println(msg)
		seq = makeValidSeq(seq)
	}

	if seq <= MinValidSequence {
		return MaxValidSequence
	}
	return seq - 1
}

// GetNextSequence
// Calculates the possible next sequence of given current sequence
// @param - currentSequence recently got sequence
// @return - valid next sequence of currentSequence
func GetNextSequence(seq int) int {
	valid, msg := checkValidInputSequence(seq)
	if !valid {
		fmt.Println(msg)
		seq = makeValidSeq(seq)
	}

	if seq >= MaxValidSequence {
		return MinValidSequence
	}
	return seq + 1
}

// DistanceBetweenSequenceNumber
// Determines if it has the shortest path to go from source to destination or from destination to source.
// If less steps required to go from source to destination then distance is positive. otherwise, negative.
// Let's say max sequence is 100, source is 51 and destination is 0. distance will be 49.
//
//	source is 50 and destination is 0. distance will be 50.
//	source is 49 and destination is 0. distance will be -49.
//
// @param source is previously got sequence
// @param destination is recently got sequence
func DistanceBetweenSequenceNumber(source, destination int) int {
	valid, msg := checkValidInputSequence(source, destination)
	if !valid {
		fmt.Println("making valid seq for : \n", msg)
		source = makeValidSeq(source)
		destination = makeValidSeq(destination)
	}

	diff := destination - source

	if getAbsValue(diff) >= MaxValidSequence/2 {
		if diff < 0 {
			diff = diff + MaxValidSequence
		} else {
			diff = diff - MaxValidSequence
		}
	}

	return diff
}

// LatestSequence
// @param - a is previously got sequence
// @param - b is recently got sequence
// @return - the actual (considering sending side's creation time) the latest sequence comparing maxValidSequence as
// lately created sequence can reach early to the receiver through network
func LatestSequence(a, b int) int {
	if a == -1 {
		valid, msg := checkValidInputSequence(b)
		if !valid {
			fmt.Println(msg)
			b = makeValidSeq(b)
		}
		return b
	}

	if b == -1 {
		valid, msg := checkValidInputSequence(a)
		if !valid {
			fmt.Println(msg)
			a = makeValidSeq(a)
		}
		return a
	}

	if DistanceBetweenSequenceNumber(a, b) > 0 {
		return b
	} else {
		return a
	}
}

// @param - a is 1st sequence
// @param - b is 2nd sequence
// @return -1 if a < b, 0 if both equal, 1 if a > b
func compareMin(a, b int) int {
	if DistanceBetweenSequenceNumber(a, b) > 0 {
		return -1
	} else if DistanceBetweenSequenceNumber(a, b) == 0 {
		return 0
	} else {
		return 1
	}
}

// CompareMax
// @param - a is 1st sequence
// @param - b is 2nd sequence
// @return 1 if a < b, 0 if both equal, -1 if a > b
func CompareMax(a, b int) int {
	if DistanceBetweenSequenceNumber(a, b) > 0 {
		return 1
	} else if DistanceBetweenSequenceNumber(a, b) == 0 {
		return 0
	} else {
		return -1
	}
}

// CountOfSequenceNumber total sequence number between range/*
func CountOfSequenceNumber(a, b int) int {
	return getAbsValue(DistanceBetweenSequenceNumber(a, b)) + 1
}

// get absolute value
func getAbsValue(a int) int {
	if a > 0 {
		return a
	} else {

		return -a
	}
}

// take invalid input sequence and map it to valid range
// this method does print warning. using it mainly for handling error and minimize the impact of failure
func makeValidSeq(seq int) int {
	valid, _ := checkValidInputSequence(seq)
	if valid {
		return seq
	}

	if seq == 0 {
		seq = MaxValidSequence
		return seq
	}

	seq = seq % MaxValidSequence

	if seq < 0 {
		seq = seq + MaxValidSequence
		return seq
	} else {
		return seq
	}
}

// SortedSequenceInsert takes an array as input and a sequence to insert in that array.
// This method assumes that the given array is already sorted, and we have to insert the given seq there.
// Instead of using max/min heap priority queue we simply start iterating from right as we are dealing with
// realtime system, and we may have to iterate only around 2-3 items on average and 1 in maximum time before
// inserting in right position which is much faster than o(log n) in max heap binary searching before insertion.
func SortedSequenceInsert(arr *[]int, seq int) (bool, error) {
	valid, msg := checkValidInputSequence(seq)
	if !valid {
		fmt.Println(msg)
		seq = makeValidSeq(seq)
	}

	if len(*arr) == 0 {
		*arr = append(*arr, seq)
		return true, nil
	}

	i := len(*arr) - 1
	for i >= 0 {
		if seq == LatestSequence((*arr)[i], seq) {
			if (*arr)[i] == seq {
				fmt.Println("blocked duplicate value insertion -> ", seq)
				return false, errors.New(fmt.Sprintf("Duplicate Input Sequence : %v", seq))
			} else {
				break
			}
		}
		i--
	}

	if i == -1 {
		*arr = append((*arr)[0:1], (*arr)[0:]...)
		(*arr)[0] = seq
	} else {
		*arr = append((*arr)[:i+1], (*arr)[i:]...)
		(*arr)[i+1] = seq
	}

	return true, nil
}
