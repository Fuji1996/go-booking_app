package main

import (
	"fmt"
	// "strings"
	"booking-app/helper"
	// "strconv"
	"time"
)

const conferenceTickets uint = 50

var conferenceName = "Go Conference"
var remainingTickets uint = conferenceTickets

//var bookings = [50]string{"fuji","jahid"}
//var bookings [10]string
//var bookings = []string{}

// var bookings = make([]map[string]string, 1) // 1 is initial size but it will grow eventually
var bookings = make([]UserData, 0)

type UserData struct {
	firstName    string
	lastName     string
	email        string
	numOfTickets uint
}

func main() {

	greetUsers()

	// fmt.Printf("conferenceTickets is : %T, remainingTickets is : %T, conferenceName is : %T\n", conferenceTickets, remainingTickets,conferenceName)

	for {

		userFirstName, userLastName, userEmail, userTickets := getUserInput()

		// var isValidName bool
		isValidName, isValidEmail, isValidTickets := helper.ValidateUserInput(userFirstName, userLastName, userEmail, userTickets, remainingTickets)

		if isValidName && isValidEmail && isValidTickets {

			bookTicket(userTickets, userFirstName, userLastName, userEmail)

			go sendTicket(userTickets, userFirstName, userLastName, userEmail)

			fmt.Printf("The first names of bookings are : %v\n", getFirstNames())

			fmt.Printf("Available tickets  now : %v\n\n", remainingTickets)

			noTicketsRemaining := remainingTickets == 0
			if noTicketsRemaining {
				//exit from here
				fmt.Printf("All %v tickets have been sold, Try again later ..! \n ", conferenceTickets)
				break
			}

		} else {
			if !isValidName {
				fmt.Println("first name or lasrt name is too short")
			}
			if !isValidEmail {
				fmt.Println("the email address you entered doesn't contain '@'")
			}
			if !isValidTickets {
				fmt.Printf("invalid ticket number")
			}
			fmt.Printf("Your input data is invalid, try again.. !\n")
		}
	}

	city := "London"

	switch city {
	case "New york":
		// execute code for New York conference tickets
	case "Singapore", "Hong Kong":
		// execute code for Singapore & Hong Kong conference tickets
	case "London", "Berlin":
		// samee code for London & Berlin conference tickets
	case "Mexico City":
		// execute code for Mexico City conference tickets

	default:
		fmt.Print("No valid city selected")

	}

}

func greetUsers() {
	fmt.Printf("Welcome to our %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v tickets are remaining\n", conferenceTickets, remainingTickets)
	fmt.Println("Get you tickets here to attend ..")
}

func getFirstNames() []string {
	firstNames := []string{}
	for _, booking := range bookings {
		//firstNames = append(firstNames, bookings[index])
		firstNames = append(firstNames, booking.firstName)
	}
	firstNames = append(firstNames)

	return firstNames
}

func getUserInput() (string, string, string, uint) {

	var userFirstName string
	var userLastName string
	var userEmail string
	var userTickets uint

	fmt.Printf("** input your first name : ")
	fmt.Scan(&userFirstName)
	fmt.Printf("** input your last name : ")
	fmt.Scan(&userLastName)
	fmt.Printf("** input your email : ")
	fmt.Scan(&userEmail)
	fmt.Printf("** how many tickets you wanna buy ? : ")
	fmt.Scan(&userTickets)
	fmt.Println()

	return userFirstName, userLastName, userEmail, userTickets

}

func bookTicket(userTickets uint, userFirstName string, userLastName, userEmail string) {
	remainingTickets = remainingTickets - userTickets

	// var userData = make(map[string]string)
	// userData["firstName"] = userFirstName
	// userData["lastName"] = userLastName
	// userData["email"] = userEmail
	// userData["numOfTickets"] = strconv.FormatUint(uint64(userTickets),10) // 10 is for base - decimal

	var userData = UserData{
		firstName:    userFirstName,
		lastName:     userLastName,
		email:        userEmail,
		numOfTickets: userTickets,
	}

	bookings = append(bookings, userData)
	fmt.Printf("List of bookings is %v\n", bookings)

	//bookings = append(bookings, userFirstName + " " + userLastName)
	// bookings[10] = userFirstName + " " + userLastName
	// fmt.Printf("the whole array : %v\n", bookings)
	// fmt.Printf("first element : %v\n", bookings[0])
	// fmt.Printf("length : %v\n", len(bookings))
	// fmt.Printf("type : %T\n", remainingTickets)

	fmt.Printf("** Congratulations, %v tickets are available .. **\n", userTickets)
	fmt.Printf("** Booking '%v' tickets for User '%v %v' confirmed..! **\n** A confirmation mail will be sent to your mail address '%v' **\n\n",
		userTickets, userFirstName, userLastName, userEmail)
	fmt.Printf("** Thank you for booking tickets with us..! **\n")
}

func sendTicket(userTickets uint, userFirstName string, userLastName, userEmail string) {
	time.Sleep(25 * time.Second)
	ticket := fmt.Sprintf("tickets %v for user '%v %v'", userTickets, userFirstName, userLastName)
	fmt.Printf("\n****************************************************************\n")
	fmt.Printf("sending : %v to '%v'\n", ticket, userEmail)
	fmt.Printf("****************************************************************\n")

}

func TestProfiling() {
	fmt.Println("starting to calculate ...")
	j := 0
	for i := 0; i < 1000000; i++ {
		j += i * i
	}
	fmt.Println("calculation finished, j = ", j)
}
